#!/bin/bash

CONFIG_PATH="$HOME/Scripts/config.sh"
BROWSER=firefox

if [[ -f $CONFIG_PATH ]]; then
    source $CONFIG_PATH
else
    exit
fi

conf=$(printf '%s\n' "${!confedit[@]}" | sort | rofi -dmenu -p "Config") 
[[ -z $conf ]] || code "${confedit[$conf]}"