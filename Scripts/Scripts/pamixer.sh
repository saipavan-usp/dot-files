MUTED=$(pamixer --get-mute)
VOLUME=$(pamixer --get-volume)

OUT=0
if [[ "$MUTED" = false ]]; then
    OUT=$VOLUME
fi

echo $OUT