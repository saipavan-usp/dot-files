#!/bin/bash

CONFIGS_PATH="Configs"
DATE=$(date +"%d-%m-%y")

declare -A paths

# Directories
paths[i3]=".config/i3"
paths[xmonad]=".xmonad"
paths[yay]=".config/yay"
paths[rofi]=".config/rofi"
paths[dunst]=".config/dunst"
paths[kitty]=".config/kitty"
paths[xmobar]=".config/xmobar"
paths[polybar]=".config/polybar"
paths[scripts]="Scripts"

# Files
paths[zshrc]=".zshrc"
paths[p10k]=".p10k.zsh"
paths[zsh_hist]=".zsh_history"
paths[picom]=".config/picom.conf"
paths[redshift]=".config/redshift.conf"

[[ ! -d "$CONFIGS_PATH" ]] && mkdir "$CONFIGS_PATH"
cd $CONFIGS_PATH

for i in "${!paths[@]}"; do
    echo "Copied $i"
    if [[ -d "$HOME/${paths[$i]}" ]]; then
        mkdir -p "${paths[$i]}"
    else
        touch "${paths[$i]}"  
    fi  
    cp -rf "$HOME/${paths[$i]}" "${paths[$i]}"
done
