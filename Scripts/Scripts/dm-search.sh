#!/bin/bash

CONFIG_PATH="$HOME/Scripts/config.sh"
BROWSER=firefox

if [[ -f $CONFIG_PATH ]]; then
    source $CONFIG_PATH
else
    exit
fi

engine=$(printf '%s\n' "${!websearch[@]}" | sort | rofi -dmenu -p "Search Engine") 
query=$(rofi -dmenu  -width 100 \
                 -l 1 -line-margin 0 -line-padding 1 \
                 -separator-style none -font "Fira Sans 12" -columns 9 -bw 0 \
                 -disable-history \
                 -hide-scrollbar \
                 -kb-row-select "Tab" -kb-row-tab "" \
                 -p "$engine")
                 
[[ -z $query ]] || $BROWSER "${websearch[$engine]}+$query"