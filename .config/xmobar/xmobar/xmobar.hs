-- Xmobar (http://projects.haskell.org/xmobar/)
-- This is one of the xmobar configurations for DTOS.
-- This config is packaged in the DTOS repo as 'dtos-xmobar'
-- Color scheme: Doom One
-- Dependencies: 
   -- otf-font-awesome 
   -- ttf-mononoki 
   -- ttf-ubuntu-font-family
   -- htop
   -- emacs
   -- pacman (Arch Linux)
   -- trayer
   -- 'dtos-local-bin' (from dtos-core-repo)

Config { font            = "xft:Ubuntu:weight=bold:pixelsize=11:antialias=true:hinting=true"
       , additionalFonts = [ 
          "xft:Mononoki:pixelsize=11:antialias=true:hinting=true"
        , "xft:Font Awesome 5 Free Solid:pixelsize=12"
        , "xft:Font Awesome 5 Brands:pixelsize=12"
        ]
       , bgColor      = "#202020"
       , fgColor      = "#ffffff"
       , position     = TopSize L 100 25
       , lowerOnStart = True
       , hideOnStart  = False
       , allDesktops  = True
       , persistent   = True
       , iconRoot     = "/home/pawan/.xmonad/xpm/"  -- default: "."
       , commands = [
                        -- Cpu usage in percent
                      Run Cpu ["-t", "<fn=2>\xf108</fn>  CPU: (<total>%)","-H","50","--high","red"] 20
                        -- Ram used number and percent
                    , Run Memory ["-t", "<fn=2>\xf233</fn>  MEM: (<usedratio>%)"] 20
                        -- Disk space free
                    , Run DiskU [("/", "<fn=2>\xf0c7</fn>  HDD: <free> free")] [] 60
                        -- Echos an "up arrow" icon in front of the uptime output.
                    , Run Com "echo" ["<fn=2>\xf0aa</fn>"] "uparrow" 3600
                        -- Uptime
                    , Run Uptime ["-t", "Uptime: <days>d <hours>h"] 360
                        -- Echos a "battery" icon in front of battery.
                    , Run Com "echo" ["<fn=2>\xf242</fn>"] "baticon" 3600
                        -- Echos a "battery" icon in front of the volume.
                    , Run Com "echo" ["<fn=2>\xf027</fn>"] "volicon" 3600
                        -- Battery
                    , Run BatteryP ["BAT1"] ["-t", "<acstatus> (<left>%)"] 1
                        -- Time and date
                    , Run Date "<fn=2>\xf017</fn>  %b %d %Y - (%H:%M) " "date" 50
                        -- Prints out the left side items such as workspaces, layout, etc.
                    , Run UnsafeStdinReader
                        -- Script that dynamically adjusts xmobar padding depending on number of trayer icons.
                    , Run Com ".config/xmobar/trayer-padding-icon.sh" [] "trayerpad" 20
                        -- Get volume percentage 
                    , Run Com "/home/pawan/Scripts/pamixer.sh" [] "volume" 1
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = " %UnsafeStdinReader% }{ \
       \<box type=Bottom width=2 mb=3 color=#ecbe7b><fc=#ecbe7b><action=`kitty -e htop`> %cpu%</action></fc></box>    \
       \<box type=Bottom width=2 mb=3 color=#ff6c6b><fc=#ff6c6b><action=`kitty -e htop`> %memory%</action></fc></box>    \
       \<box type=Bottom width=2 mb=3 color=#98be65><fc=#98be65> %uparrow% %uptime%</fc></box>    \
       \<box type=Bottom width=2 mb=3 color=#da8548><fc=#da8548> %baticon% %battery%</fc></box>   \
       \<box type=Bottom width=2 mb=3 color=#46d9ff><fc=#46d9ff> %date%</fc></box>   \
       \<box type=Bottom width=2 mb=3 color=#98be65><fc=#98be65> %volicon% VOL: %volume%</fc></box>    \
       \%trayerpad%"
    --    \<box type=Bottom width=2 mb=2 color=#a9a1e1><fc=#a9a1e1> %disku%</fc></box>    \
       }
