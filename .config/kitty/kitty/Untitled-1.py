# class UserMainCode(object):
#     @classmethod
#     def maxRacers(cls, input1):
#         input2 = []
#         for i in input1:
#             for j in range(i[0], i[1]):
#                 input2.append(j)
#         print(input2)
#         dic = {}
#         for i in input2:
#             dic[i] = dic.get(i, 0) + 1
#         return max(dic.values())

#     def Max_Posiible(cls, input1, input2):
#         dic = {}
#         dic["1"] = 0
#         dic["2"] = 0
#         dic["3"] = 0
#         for i in input1:
#             dic[i] = dic.get(i, 0) + 1
#         if dic["1"] and dic["2"] and dic["3"]:
#             return min(dic["1"], dic["2"], dic["3"])
#         return 0
        
        
#     def findMaximum(cls, input1, input2, input3):
#         res = 0
#         for input2, v in zip(input1, list(input2)):
#             if v == "P":
#                 res += input2
#             else:
#                 res -= input2
#         return abs(res) * 100
    
    
#     def pushZeroesEnd(cls, input1, input2):
#         count = 0
#         for i in range(input2):
#             if input1[i] != 0:
#                 input1[count] = input1[i]
#                 count+=1
#         while count < input2:
#             input1[count] = 0
#             count += 1
#         return input1
        
        
#     def removingOcuurences(cls, input1, input2):
#         print("".join([i for i in input1 if i != input2]))
        

#     def countPalindromes(cls, input1, input2):
#         ans = [i for i in input1.split() if i == i[::-1]]
#         return len(ans)

#     def removeDuplicates(cls, input1, input2):
#         dic = {}
#         for i in input1:
#             dic[i] = dic.get(i, 0) + 1
#         return list(dic.keys())

#     def autoComplete(cls, input1, input2):
#         res = []
#         for i in input1.split():
#             if i.startswith(input2):
#                 res.append(i)
#         return res

#     def noOfSubsets(cls, input1, input2, input3):
#         total = 1 << input1;
#         res = 0
#         for i in range(total):
#             sum = 0
#             for j in range(input1):
#                 if (i and (1 << j)) != 0:
#                     sum += input2[j]
#                     if sum % input3 == 0:
#                         res += 1
#         return res

#     def maxElement(cls, input1):
#         dic = {}
#         mx = 0
#         res = ""
#         for i in input1:
#             dic[i] = dic.get(i, 0) + 1
#             if dic[i] > mx:
#                 res = i
#                 mx = dic[i]
#         if list(dic.values()).count(mx) > 1:
#             return 0
#         return res

#     def breakShell(cls, input1):
#         return sum(range(3, input1))

#     def happyNumber(cls, input1, input2):
#         def compute(num):
#             while num > 10:
#                 temp = 0
#                 for i in str(num):
#                     temp += int(i) * int(i)
#                 num = temp
#                 if num == 1:
#                     return 1
#             return 0

#         res = []
#         for i in input2:
#             res.append(compute(i))
#         return res




# obj = UserMainCode()
# print(obj.maxRacers([[1,3], [2,5], [2,4], [3,5]]))
# print(obj.maxRacers([[1,7], [2,4], [6,9], [3,8], [5,10]]))
# print(obj.Max_Posiible("012312333", 4))
# print(obj.findMaximum([4,3,5], "PNP", 3))
# print(obj.pushZeroesEnd([0,3,0,2,0], 5))
# print(obj.removingOcuurences("lord of the rings", "o"))
# print(obj.countPalindromes("this is level 71", 11))
# print(obj.removeDuplicates([11,11,11,13,13,20], 6))
# print(obj.autoComplete("this is the introductory information of new word search engine pollyhop", "in"))
# print(obj.noOfSubsets(5,[1,2,3,4,5], 5))
# print(obj.breakShell(12))
# print(obj.maxElement("abcddd"))
# print(obj.happyNumber(1, [11,19]))

for _ in range(int(input())):
    res = 0
    n = int(input())
    z = int(input())
    p = int(input())
    for i in range(1, n):
        l, r = str(z)[:i], str(z)[i:]
        if l.startswith("0") or r.startswith("0") or int(l)%p != 0 or int(r)%p != 0:
            continue
        else:
            res += 1
    print(res)